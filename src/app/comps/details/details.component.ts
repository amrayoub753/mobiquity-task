import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { ApiService } from 'src/app/shared/service/api.service';
import { Season, } from './../../interfaces/season.interface'
import { Race } from './../../interfaces/winners.interface'

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss']
})
export class DetailsComponent implements OnInit {
  seasonYear: number
  seasons: Season[];
  winners: Race[];
  step: number = -1;
  cols: number = 2;
  mainColSpan: number = 1;
  mainRowSpan: number = 3;
  mapRowSpan: number = 3;
  rowHeight: string = '3:1';

  constructor(
    private route: ActivatedRoute,
    private apiService: ApiService
  ) {
    this.route.params.subscribe((param: Params) => {
      this.seasonYear = param.year;
    });
  }

  ngOnInit(): void {
    this.getSeason(this.seasonYear);
    this.getWinners(this.seasonYear);
    this.resizeGrid(window.screen.width);
  }

  getSeason(season: number) {
    this.apiService.getSeason(season)
      .subscribe(season => this.seasons = season);
  };

  getWinners(season: number) {
    this.apiService.getWinners(season)
      .subscribe(winners => this.winners = winners)
  }

  setStep(index: number) {
    this.step = index;
  }

  nextStep() {
    this.step++;
  }

  prevStep() {
    this.step--;
  }

  onResize(event) {
    this.resizeGrid(event.target.innerWidth);
  }

  resizeGrid(windowWidth: number): void {
    switch (true) {
      case windowWidth < 1200: {
        this.mainRowSpan = 5;
        this.mapRowSpan = 3;
        this.rowHeight = '4:1';
        break;
      }
      case windowWidth < 950: {
        this.cols = 1;
        this.mainColSpan = 1;
        this.mainRowSpan = 4;
        this.mapRowSpan = 3;
        break;
      }

      case windowWidth < 840: {
        this.mainRowSpan = 6;
        this.mapRowSpan = 6;
        this.rowHeight = '7:1';
        break;
      }

      case windowWidth < 700: {
        this.mainRowSpan = 10;
        this.mapRowSpan = 10;
        this.rowHeight = '10:1';
        break;
      }

      case windowWidth < 620: {
        this.mainRowSpan = 15;
        this.mapRowSpan = 15;
        this.rowHeight = '12:1';
        break;
      }

      case windowWidth < 500: {
        this.mainRowSpan = 20;
        this.mapRowSpan = 20;
        this.rowHeight = '12:1';
        break;
      }
      case windowWidth < 400: {
        this.mainRowSpan = 20;
        this.mapRowSpan = 20;
        this.rowHeight = '9:1';
        break;
      }
      default:
        this.cols = 2;
        this.mainColSpan = 1;
        this.mainRowSpan = 3;
        this.mapRowSpan = 3;
        this.rowHeight = '3:1';
    }
  }
}
