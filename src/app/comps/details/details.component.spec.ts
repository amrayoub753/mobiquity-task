import { async, ComponentFixture, TestBed, inject } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientModule } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DetailsComponent } from './details.component';
import {
  MatBadgeModule,
  MatDividerModule,
  MatExpansionModule,
  MatGridListModule,
  MatIconModule,
  MatListModule,
  MatToolbarModule,
  MatProgressSpinnerModule
} from '@angular/material';
import { ApiService } from 'src/app/shared/service/api.service';


describe('DetailsComponent', () => {
  let component: DetailsComponent;
  let fixture: ComponentFixture<DetailsComponent>;
  let service: ApiService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports:[
        RouterTestingModule,
        HttpClientModule,

        BrowserAnimationsModule,
        MatBadgeModule,
        MatToolbarModule,
        MatExpansionModule,
        MatGridListModule,
        MatIconModule,
        MatListModule,
        MatDividerModule,
        MatProgressSpinnerModule
      ],
      declarations: [ DetailsComponent ],
      providers: [
        ApiService      ]
    })
    .compileComponents().then(() => {
      fixture = TestBed.createComponent(DetailsComponent);
      component = fixture.componentInstance;
      service = TestBed.get(ApiService);
      fixture.detectChanges();
    })
  }));
});
