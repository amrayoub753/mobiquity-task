import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SeasonsComponent } from './seasons/seasons.component';
import { DetailsComponent } from './details/details.component';
import { compsRoutingModule} from './comps-routing.module';
import { ViewsModule } from '../views/views.module';
import { HomeComponent } from './home/home.component';
import { MaterialModule } from '../material/material.module';



@NgModule({
  declarations: [SeasonsComponent, DetailsComponent, HomeComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],

  imports: [
    CommonModule,
    compsRoutingModule,
    ViewsModule,
    MaterialModule
  ]
})
export class CompsModule { }
