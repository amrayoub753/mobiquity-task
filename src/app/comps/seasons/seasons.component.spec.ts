import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientModule } from '@angular/common/http';
import { RouterTestingModule } from '@angular/router/testing';
import {
  MatBadgeModule,
  MatDividerModule,
  MatExpansionModule,
  MatGridListModule,
  MatIconModule,
  MatListModule,
  MatToolbarModule,
  MatProgressSpinnerModule
} from '@angular/material';

import { SeasonsComponent } from './seasons.component';
import { ApiService } from 'src/app/shared/service/api.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';


describe('SeasonsComponent', () => {
  let component: SeasonsComponent;
  let fixture: ComponentFixture<SeasonsComponent>;
  let service: ApiService;


  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SeasonsComponent ],
      imports: [
        RouterTestingModule,
        HttpClientModule,

        BrowserAnimationsModule,
        MatBadgeModule,
        MatToolbarModule,
        MatExpansionModule,
        MatGridListModule,
        MatIconModule,
        MatListModule,
        MatDividerModule,
        MatProgressSpinnerModule
      ],
      providers: [
        ApiService
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SeasonsComponent);
    component = fixture.componentInstance;
    service = TestBed.get(ApiService)
    fixture.detectChanges();
    component.isLoaded = true;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
    expect(service).toBeTruthy();
    expect(service instanceof ApiService).toBeTruthy()
  });

  it('should get all champions from 2005 to 2015 (11)', () => {
    spyOn(component, 'ngOnInit').and.callThrough()
    component.ngOnInit()
    fixture.detectChanges();

    fixture.whenStable().then(() => {
      expect(component.Seasons.length).toEqual(11);
    })
  });
});
