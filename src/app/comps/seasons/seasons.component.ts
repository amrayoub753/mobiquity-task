import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/shared/service/api.service';
import { Router } from '@angular/router';
import { Season } from './../../interfaces/season.interface'

@Component({
  selector: 'app-seasons',
  templateUrl: './seasons.component.html',
  styleUrls: ['./seasons.component.scss']
})
export class SeasonsComponent implements OnInit {
  isLoaded: boolean = false;
  Seasons: Season[];

  constructor(private api: ApiService, private router: Router) {
  }

  ngOnInit(): void {
    this.api.getAllSeasons()
    .subscribe(seasons => {
      this.Seasons = seasons;
      this.isLoaded = true;
    });
  }

  goToDetails(season) {
    this.router.navigateByUrl(`/seasons/${season}`);
  }

}
