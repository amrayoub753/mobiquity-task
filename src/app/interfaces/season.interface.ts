export interface Season {
  season: string;
  rounds: string;
  DriverStandings: Winner
}

export interface Winner {
  position: string;
  positionText: string;
  points: string;
  wins: string;
  Driver: Driver;
  Constructors: Constructor[];
}

export interface Driver {
  driverId: string;
  permanentNumber: string;
  code: string;
  url: string;
  givenName: string;
  familyName: string;
  dateOfBirth: string;
  nationality: string;
}


export interface RootObject {
  MRData: MRData;
}

export interface MRData {
  xmlns: string;
  series: string;
  url: string;
  limit: string;
  offset: string;
  total: string;
  StandingsTable: StandingsTable;
}

export interface StandingsTable {
  driverStandings: string;
  StandingsLists: Season[];
}


export interface Constructor {
  constructorId: string;
  url: string;
  name: string;
  nationality: string;
}
