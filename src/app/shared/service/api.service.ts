import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs'
import { catchError, map } from 'rxjs/operators';
import { Season } from './../../interfaces/season.interface';
import { Race } from './../../interfaces/winners.interface'
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  private API_URL: string = environment.apiUrl;
  private offset: number = 55;
  private limit: number  = 11;

  constructor(private http: HttpClient) { }

  getSeason(season: number): Observable<Season[]> {
    return this.http.get<Season[]>(`${this.API_URL}${season}/driverStandings/1.json`)
      .pipe(
        map((res: any) => res.MRData.StandingsTable.StandingsLists),
        catchError(this.handleError())
      )
  };

  getAllSeasons(): Observable<Season[]> {
    return this.http.get<Season[]>(`${this.API_URL}driverStandings/1.json?offset=${this.offset}&limit=${this.limit}`)
      .pipe(
        map((res: any) => res.MRData.StandingsTable.StandingsLists ),
        catchError(this.handleError())
      )
  };

  getWinners(season: number): Observable<Race[]> {
    return this.http.get<Race[]>(`${this.API_URL}${season}/results/1.json`)
      .pipe(
        map((res: any) => res.MRData.RaceTable.Races),
        catchError(this.handleError())
      )
  };

  private handleError<T>(result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);
      return of(result as T);
    };
  }
}
