import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DetailsComponent } from './comps/details/details.component';
import { SeasonsComponent } from './comps/seasons/seasons.component';


const routes: Routes = [
  {
    path: '', redirectTo: 'seasons' ,pathMatch: 'full'
  },
  {
    path: 'seasons' , loadChildren: () =>
    import('./comps/comps.module').then((m) => m.CompsModule)
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

